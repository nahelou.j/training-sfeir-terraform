variable "hello" {
  type        = string
  description = "the value to print"
}

resource "null_resource" "foo" {
  triggers = {
    foo = var.hello
  }
}

output "foo" {
  value = null_resource.foo.triggers.foo
}
